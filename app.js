import express from 'express';
import { fileURLToPath } from 'url';
import path from 'path';

// Importa directamente el router aquí
import router from './router/index.js';

const puerto = 80; // Cambiado de 80 a 3000 para propósitos de desarrollo
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const app = express();

app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.urlencoded({ extended: true }));

// Usa el router directamente aquí
app.use(router);

app.listen(puerto, () => {
  console.log(`Servidor corriendo en http://localhost:${puerto}`);
});


